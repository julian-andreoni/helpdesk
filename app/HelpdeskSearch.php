<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HelpdeskSearch extends Model
{
    const UPDATED_AT = null;

    protected $table = 'helpdesk_search';

    protected $dates = ['created_at'];

    protected $fillable = ['helpdesk_id','search'];

    public function getStatisticsByHelpdesk(int $helpdesk_id){
        return DB::table('helpdesk_search')
                     ->select(DB::raw('count(*) as cant, search'))
                     ->where('helpdesk_id', $helpdesk_id)
                     ->groupBy('search')
                     ->orderByRaw('cant DESC')
                     ->limit(5)
                     ->get();
    }

    public function getStatistics(){
        return DB::table('helpdesk_search')
                     ->select(DB::raw('count(*) as cant, search'))
                     ->groupBy('search')
                     ->orderByRaw('cant DESC')
                     ->limit(5)
                     ->get();
    }
    
}
