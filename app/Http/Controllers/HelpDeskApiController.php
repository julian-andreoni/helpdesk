<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\IncidentsRequest;
use App\Http\Requests\StatisticsRequest;
use GuzzleHttp\Client;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\HelpdeskSearch;

class HelpDeskApiController extends Controller
{
    /**
     * Search incidents for a helpdesk.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(IncidentsRequest $params)
    {
        if(!$sock = @fsockopen('www.google.com', 80)){
            throw new HttpResponseException(response()->json('There is no Internet connection', 422));
        }

        $helpdesk_id = $params->helpdesk_id;
        $text_to_search = trim(strtolower($params->text_to_search));
        $detailed = $params->detailed;

        $api_user = env('API_USER', null);
        $api_pass = env('API_PASS', null);
        $api_uri = env('API_URI', null);

        if(!$api_user || !$api_pass || !$api_uri){
            throw new HttpResponseException(response()->json('Missing configuration data for the API', 422));
        }

        $client = new Client();

        try {
            $getIncidents = $client->request('GET', sprintf('%s%s%d',$api_uri,'incidents.by.helpdesk?helpdesk_id=',$helpdesk_id), [
                'auth' => [$api_user, $api_pass]
            ]);
        } catch (\Throwable $th) {
            throw new HttpResponseException(response()->json('An error occurred connecting to the API', 423));
        }      

        $incidents = json_decode($getIncidents->getBody());
        
        $response = [];
        $response['ok'] = false;
        $response['incidents'] = [];
        
        if($incidents->requestIds){
            $response['msj'] = 'There are no incidents with the requested text';
            foreach ($incidents->requestIds as $id_incident) {
                try {
                    $incidentDetailRequest = $client->request('GET', sprintf('%s%s%d',$api_uri,'incident?id=',$id_incident), [
                        'auth' => [$api_user, $api_pass]
                    ]);
                } catch (\Throwable $th) {
                    throw new HttpResponseException(response()->json('An error occurred connecting to the API', 423));
                }

                $incidentDetail = json_decode($incidentDetailRequest->getBody());

                if(!$text_to_search || stristr($incidentDetail->description, $text_to_search)) {
                    $response['ok'] = true;//If there is at least a incident..
                    $response['msj'] = '';
                    if($detailed){
                        $incidentDetail->description = strip_tags($incidentDetail->description);
                        $response['incidents'][] = $incidentDetail;
                    }else{
                        $response['incidents'][] = ['id'=>$incidentDetail->id];
                    }
                }
            }
            if($text_to_search){
                $helpdesk_search = HelpdeskSearch::create(['search' => $text_to_search,'helpdesk_id' => $helpdesk_id]);
            }
        }else{
            $response['msj'] = 'This helpdesk does not exist or there are no registered incidents';
        }

        return json_encode($response);

    }

    public function statistics(StatisticsRequest $params){
        $response = [];
        $response['ok'] = true;

        $helpdesk_id = (int)$params->helpdesk_id;
        $helpdesk = new HelpdeskSearch;

        if($helpdesk_id)
            $response['statistics'] = $helpdesk->getStatisticsByHelpdesk($helpdesk_id);
        else
            $response['statistics'] = $helpdesk->getStatistics();

        if($response['statistics']->isEmpty()){
            $response['ok'] = false;
            $response['msj'] = 'There are no statistics';
        }

        return json_encode($response);
    }
}