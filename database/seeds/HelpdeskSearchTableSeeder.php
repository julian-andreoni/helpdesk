<?php

use Illuminate\Database\Seeder;

class HelpdeskSearchTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('helpdesk_search')->delete();
        
        \DB::table('helpdesk_search')->insert(array (
            0 => 
            array (
                'id' => 1,
                'helpdesk_id' => 11,
                'search' => 'Do you have a tutorial',
                'created_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'helpdesk_id' => 11,
                'search' => 'Do you have a tutorial',
                'created_at' => '2019-01-13 01:02:53',
            ),
            2 => 
            array (
                'id' => 3,
                'helpdesk_id' => 12,
                'search' => 'Do you have a tutorial',
                'created_at' => '2019-01-13 01:07:56',
            ),
            3 => 
            array (
                'id' => 4,
                'helpdesk_id' => 13,
                'search' => 'Do you have a tutorial',
                'created_at' => '2019-01-13 01:09:05',
            ),
            4 => 
            array (
                'id' => 5,
                'helpdesk_id' => 11,
                'search' => 'linux',
                'created_at' => '2019-01-13 01:49:10',
            ),
            5 => 
            array (
                'id' => 6,
                'helpdesk_id' => 11,
                'search' => 'windows',
                'created_at' => '2019-01-13 01:55:39',
            ),
            6 => 
            array (
                'id' => 7,
                'helpdesk_id' => 11,
                'search' => 'linux',
                'created_at' => '2019-01-13 01:58:51',
            ),
            7 => 
            array (
                'id' => 8,
                'helpdesk_id' => 11,
                'search' => 'mac',
                'created_at' => '2019-01-13 01:59:06',
            ),
            8 => 
            array (
                'id' => 9,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 01:59:26',
            ),
            9 => 
            array (
                'id' => 10,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 02:03:40',
            ),
            10 => 
            array (
                'id' => 11,
                'helpdesk_id' => 11,
                'search' => 'asus',
                'created_at' => '2019-01-13 02:04:31',
            ),
            11 => 
            array (
                'id' => 12,
                'helpdesk_id' => 11,
                'search' => 'asus',
                'created_at' => '2019-01-13 02:04:50',
            ),
            12 => 
            array (
                'id' => 13,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 02:48:25',
            ),
            13 => 
            array (
                'id' => 14,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 04:40:22',
            ),
            14 => 
            array (
                'id' => 15,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 21:09:07',
            ),
            15 => 
            array (
                'id' => 16,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 21:12:48',
            ),
            16 => 
            array (
                'id' => 17,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 22:13:21',
            ),
            17 => 
            array (
                'id' => 18,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 22:18:24',
            ),
            18 => 
            array (
                'id' => 19,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 22:19:31',
            ),
            19 => 
            array (
                'id' => 20,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 22:22:34',
            ),
            20 => 
            array (
                'id' => 21,
                'helpdesk_id' => 11,
                'search' => 'controller',
                'created_at' => '2019-01-13 22:25:52',
            ),
            21 => 
            array (
                'id' => 22,
                'helpdesk_id' => 11,
                'search' => 'windows',
                'created_at' => '2019-01-13 22:27:04',
            ),
            22 => 
            array (
                'id' => 23,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-13 23:26:57',
            ),
            23 => 
            array (
                'id' => 24,
                'helpdesk_id' => 11,
                'search' => 'do you have a tutorial',
                'created_at' => '2019-01-14 00:24:58',
            ),
            24 => 
            array (
                'id' => 25,
                'helpdesk_id' => 11,
                'search' => 'windows',
                'created_at' => '2019-01-14 03:13:40',
            ),
            25 => 
            array (
                'id' => 26,
                'helpdesk_id' => 11,
                'search' => 'windows',
                'created_at' => '2019-01-14 05:11:29',
            ),
            26 => 
            array (
                'id' => 27,
                'helpdesk_id' => 11,
                'search' => 'windows',
                'created_at' => '2019-01-14 05:11:56',
            ),
            27 => 
            array (
                'id' => 28,
                'helpdesk_id' => 11,
                'search' => 'Do you have a tutorial',
                'created_at' => '2019-01-14 05:46:26',
            ),
            28 => 
            array (
                'id' => 29,
                'helpdesk_id' => 11,
                'search' => 'Do you have a tutorial',
                'created_at' => '2019-01-14 05:46:27',
            ),
            29 => 
            array (
                'id' => 30,
                'helpdesk_id' => 1,
                'search' => 'search',
                'created_at' => '2019-01-14 06:14:17',
            ),
            30 => 
            array (
                'id' => 31,
                'helpdesk_id' => 1,
                'search' => 'search',
                'created_at' => '2019-01-14 06:14:30',
            ),
            31 => 
            array (
                'id' => 32,
                'helpdesk_id' => 1,
                'search' => 'search',
                'created_at' => '2019-01-14 06:14:48',
            ),
            32 => 
            array (
                'id' => 33,
                'helpdesk_id' => 221,
                'search' => 'searrrrch',
                'created_at' => '2019-01-14 06:15:35',
            ),
            33 => 
            array (
                'id' => 34,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:16:52',
            ),
            34 => 
            array (
                'id' => 35,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:17:17',
            ),
            35 => 
            array (
                'id' => 36,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:17:20',
            ),
            36 => 
            array (
                'id' => 37,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:17:41',
            ),
            37 => 
            array (
                'id' => 38,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:20:35',
            ),
            38 => 
            array (
                'id' => 39,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:23:42',
            ),
            39 => 
            array (
                'id' => 40,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:24:15',
            ),
            40 => 
            array (
                'id' => 41,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:27:06',
            ),
            41 => 
            array (
                'id' => 42,
                'helpdesk_id' => 10,
                'search' => 'Windows',
                'created_at' => '2019-01-14 06:27:24',
            ),
        ));
        
        
    }
}