<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Julian',
                'email' => 'julian.andreoni@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$e4aQdyMVZgb0rl5scadineUb59epZGP0FuLX53MyTfcSPa1oOTThK',
                'remember_token' => 'aqkACcyLCFbhHfNmuh205gq7C7fb6sXyj8tw2YlWUyPUDhqPaz5kIZxuzlZn',
                'created_at' => '2019-01-13 02:47:22',
                'updated_at' => '2019-01-13 02:47:22',
            ),
        ));
        
        
    }
}