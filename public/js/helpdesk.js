class Helpdesk{

	search(){
		var helpdesk_id = parseInt($('#helpdesk_id').val());

		$.ajax({
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: '/api/incidents',
			type: 'GET',
			data:{
				helpdesk_id: helpdesk_id,
				detailed: 1
			},
			beforeSend: function(){
				$("#loading").css("display", "inline");
				$("#msg_alert,#search_result").css("display", "none");
				$("#submit").attr("disabled", true); 
			},
			success: function(response){

				$("#submit").attr("disabled", false); 
				$("#loading").css("display", "none");
				var res = JSON.parse(response);
				if(res.ok){
					$('#myTable tbody').empty();
					res.incidents.forEach(element => {
						$('#myTable tbody').append("<tr><td>"+element.id+"</td><td>"+element.title+"</td><td>"+element.description+"</td></tr>");
					});
					$.getScript("js/searchTable.js");
					$("#search_result").css("display", "block");
				}else{
					$("#msg_alert").html(res.msj);
					$("#msg_alert").css("display", "block");
				}	
			},
			error:function(response){
				console.log(response['responseText']);
			}
		})
	}

}

var h = new Helpdesk();