@extends('layouts.helpdesk')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Incidents</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- <div class="form-group form-float"> -->
                    <form action="#" onsubmit="h.search()">
                        <div class="input-group col-md-6">
                            <input id="helpdesk_id" class="form-control width100" type="number" min="1" placeholder="Enter helpdesk id..." required>
                            <span class="input-group-btn">
                            <button class="btn btn-info" id="submit">Send</button>
                            </span>
                            <span id="loading" class="label label-default" style="display:none;">Please wait...</span>                            
                        </div>
                    </form>
                    <!-- </div> -->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="msg_alert" class="alert alert-warning" role="alert" style="display:none;">
            </div>
            <div id="search_result" class="card" style="display:none;">
                <input id="text_search" onkeyup="searchDescription()" class="form-control width100" type="text" placeholder="Enter text to search...">
                <div class="table-responsive">
                    <table class="table" id="myTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('javascripts')
    <script src="../js/helpdesk.js"></script>    
@endsection