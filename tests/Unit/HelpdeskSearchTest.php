<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\HelpdeskSearch;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpDeskSearchTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function can_create_a_helpdeskSearch()
    {
        $data = [
            'search' => 'Windows',
            'helpdesk_id' => 10
        ];
      
        $helpdeskSearch = HelpdeskSearch::create(['search' => 'Windows','helpdesk_id' => 10]);

        $this->assertInstanceOf(HelpdeskSearch::class, $helpdeskSearch);
        $this->assertEquals($data['search'], $helpdeskSearch->search);
        $this->assertEquals($data['helpdesk_id'], $helpdeskSearch->helpdesk_id);
    }
}
